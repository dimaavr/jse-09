package ru.tsc.avramenko.tm.api;

public interface ICommandController {
    void showErrorCommand();

    void showErrorArgument();

    void exit();

    void showInfo();

    void showAbout();

    void showVersion();

    void showCommands();

    void showArguments();

    void showCommandValue(String value);

    void showHelp();
}
