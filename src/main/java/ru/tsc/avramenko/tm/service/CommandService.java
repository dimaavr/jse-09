package ru.tsc.avramenko.tm.service;

import ru.tsc.avramenko.tm.api.ICommandRepository;
import ru.tsc.avramenko.tm.api.ICommandService;
import ru.tsc.avramenko.tm.model.Command;

public class CommandService implements ICommandService {

    private final ICommandRepository commandRepository;

    public CommandService(final ICommandRepository commandRepository) {
        this.commandRepository = commandRepository;
    }

    @Override
    public Command[] getTerminalCommands() {
        return commandRepository.getTerminalCommands();
    }

}
